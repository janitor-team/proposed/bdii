From 2480948817911d79f8445308a7995da138849729 Mon Sep 17 00:00:00 2001
From: Mattias Ellert <mattias.ellert@physics.uu.se>
Date: Thu, 24 Dec 2020 10:37:36 +0100
Subject: [PATCH] Update default paths (#31)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Use /run instead of /var/run
Use /run/lock instead of /var/lock

Filesystem Hierarchy Standard 3.0 says about /var/run:
"This directory was once intended for system information data
describing the system since it was booted. These functions have been
moved to /run."

Using /var/run in systemd unit files results in warnings in the logs:

$ systemctl status bdii | cat
● bdii.service - Berkeley Database Information Index
     Loaded: loaded (/usr/lib/systemd/system/bdii.service; disabled; vendor preset: disabled)
     Active: inactive (dead)
       Docs: man:bdii-update(1)

Dec 23 07:15:32 localhost.localdomain systemd[1]: /usr/lib/systemd/system/bdii.service:10: PIDFile= references a path below legacy directory /var/run/, updating /var/run/bdii/bdii-update.pid → /run/bdii/bdii-update.pid; please update the unit file accordingly.
---
 Makefile                |  2 +-
 bin/bdii-update         |  2 +-
 etc/bdii-slapd.conf     |  4 ++--
 etc/bdii-top-slapd.conf |  4 ++--
 etc/bdii.conf           |  2 +-
 etc/init.d/bdii         | 18 +++++++++---------
 6 files changed, 16 insertions(+), 16 deletions(-)

diff --git a/Makefile b/Makefile
index 35acefe..cc379e8 100644
--- a/Makefile
+++ b/Makefile
@@ -12,7 +12,7 @@ default:
 install:
 	@echo installing ...
 	@mkdir -p $(prefix)/usr/sbin/
-	@mkdir -p $(prefix)/var/run/bdii/
+	@mkdir -p $(prefix)/run/bdii/
 	@mkdir -p $(prefix)/var/lib/bdii/gip/ldif/
 	@mkdir -p $(prefix)/var/lib/bdii/gip/provider/
 	@mkdir -p $(prefix)/var/lib/bdii/gip/plugin/
diff --git a/bin/bdii-update b/bin/bdii-update
index bce8f19..32d9452 100755
--- a/bin/bdii-update
+++ b/bin/bdii-update
@@ -75,7 +75,7 @@ def get_config(config):
         config['BDII_DAEMON'] = False
 
     if 'BDII_RUN_DIR' not in config:
-        config['BDII_RUN_DIR'] = '/var/run/bdii'
+        config['BDII_RUN_DIR'] = '/run/bdii'
 
     if 'BDII_PID_FILE' not in config:
         config['BDII_PID_FILE'] = "%s/bdii-update.pid" % config['BDII_RUN_DIR']
diff --git a/etc/bdii-slapd.conf b/etc/bdii-slapd.conf
index 115eb67..841dbf3 100644
--- a/etc/bdii-slapd.conf
+++ b/etc/bdii-slapd.conf
@@ -11,8 +11,8 @@ include /etc/ldap/schema/GLUE20.schema
 
 allow bind_v2 
 
-pidfile         /var/run/bdii/db/slapd.pid
-argsfile        /var/run/bdii/db/slapd.args
+pidfile         /run/bdii/db/slapd.pid
+argsfile        /run/bdii/db/slapd.args
 loglevel        0
 idletimeout     120
 sizelimit       unlimited
diff --git a/etc/bdii-top-slapd.conf b/etc/bdii-top-slapd.conf
index 9df2958..c4113bb 100644
--- a/etc/bdii-top-slapd.conf
+++ b/etc/bdii-top-slapd.conf
@@ -11,8 +11,8 @@ include /etc/ldap/schema/GLUE20.schema
 
 allow bind_v2 
 
-pidfile         /var/run/bdii/db/slapd.pid
-argsfile        /var/run/bdii/db/slapd.args
+pidfile         /run/bdii/db/slapd.pid
+argsfile        /run/bdii/db/slapd.args
 loglevel        0
 idletimeout     120
 sizelimit       unlimited
diff --git a/etc/bdii.conf b/etc/bdii.conf
index 8958346..4986450 100644
--- a/etc/bdii.conf
+++ b/etc/bdii.conf
@@ -1,5 +1,5 @@
 BDII_LOG_FILE=/var/log/bdii/bdii-update.log
-BDII_PID_FILE=/var/run/bdii/bdii-update.pid
+BDII_PID_FILE=/run/bdii/bdii-update.pid
 BDII_LOG_LEVEL=ERROR
 BDII_LDIF_DIR=/var/lib/bdii/gip/ldif
 BDII_PROVIDER_DIR=/var/lib/bdii/gip/provider
diff --git a/etc/init.d/bdii b/etc/init.d/bdii
index c9f8a01..e81fc13 100755
--- a/etc/init.d/bdii
+++ b/etc/init.d/bdii
@@ -37,11 +37,11 @@ log_failure_msg()
 
 prog=bdii
 
-# Debian does not have /var/lock/subsys
-if [ -d /var/lock/subsys ] ; then
-    LOCK_DIR=/var/lock/subsys
+# Debian does not have /run/lock/subsys
+if [ -d /run/lock/subsys ] ; then
+    LOCK_DIR=/run/lock/subsys
 else
-    LOCK_DIR=/var/lock
+    LOCK_DIR=/run/lock
 fi
 
 lockfile=${LOCK_DIR}/$prog    # TODO is it necessary??
@@ -69,7 +69,7 @@ fi
 
 UPDATE_LOCK_FILE=${UPDATE_LOCK_FILE:-${LOCK_DIR}/bdii-update}
 SLAPD_LOCK_FILE=${SLAPD_LOCK_FILE:-${LOCK_DIR}/bdii-slapd}
-UPDATE_PID_FILE=${BDII_PID_FILE:-/var/run/bdii/bdii-update.pid}
+UPDATE_PID_FILE=${BDII_PID_FILE:-/run/bdii/bdii-update.pid}
 BDII_USER=${BDII_USER:-ldap}
 BDII_VAR_DIR=${BDII_VAR_DIR:-/var/lib/bdii}
 BDII_UPDATE=${BDII_UPDATE:-/usr/sbin/bdii-update}
@@ -80,7 +80,7 @@ SLAPD_PORT=${SLAPD_PORT:-2170}
 BDII_IPV6_SUPPORT=${BDII_IPV6_SUPPORT:-no}
 SLAPD_HOST6=${SLAPD_HOST6:-::}
 SLAPD_DB_DIR=${SLAPD_DB_DIR:-$BDII_VAR_DIR/db}
-SLAPD_PID_FILE=${SLAPD_PID_FILE:-/var/run/bdii/db/slapd.pid}
+SLAPD_PID_FILE=${SLAPD_PID_FILE:-/run/bdii/db/slapd.pid}
 DB_CONFIG=${DB_CONFIG:-/etc/bdii/DB_CONFIG}
 DELAYED_DELETE=${DELAYED_DELETE:-${BDII_VAR_DIR}/delayed_delete.pkl}
 BDII_RAM_SIZE=${BDII_RAM_SIZE:-1500M}
@@ -185,9 +185,9 @@ function start(){
     chown -R ${BDII_USER}:${BDII_USER} ${SLAPD_DB_DIR}
     [ -x /sbin/restorecon ] && /sbin/restorecon -R ${BDII_VAR_DIR}
 
-    mkdir -p /var/run/bdii/db
-    chown -R ${BDII_USER}:${BDII_USER} /var/run/bdii
-    [ -x /sbin/restorecon ] && /sbin/restorecon -R /var/run/bdii/db
+    mkdir -p /run/bdii/db
+    chown -R ${BDII_USER}:${BDII_USER} /run/bdii
+    [ -x /sbin/restorecon ] && /sbin/restorecon -R /run/bdii/db
 
     $RUNUSER -s /bin/sh ${BDII_USER} -c "rm -f ${SLAPD_DB_DIR}/stats/* 2>/dev/null"
     $RUNUSER -s /bin/sh ${BDII_USER} -c "rm -f ${SLAPD_DB_DIR}/glue/* 2>/dev/null"
-- 
2.34.1

